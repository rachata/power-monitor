import 'dart:async';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:pretty_gauge/pretty_gauge.dart';

class ACPage extends StatefulWidget {
  const ACPage({Key? key}) : super(key: key);

  @override
  _ACPageState createState() => _ACPageState();
}

class _ACPageState extends State<ACPage> {

  double current = 0.0;
  double energy = 0.0;
  double power = 0.0;
  double voltage = 0.0;


  final databaseReference = FirebaseDatabase.instance.reference();

  @override
  void initState() {
    super.initState();

     Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        callData();
      });
    });
  }

  callData(){
    databaseReference.child("/PowerBox/AC/Current").once().then((value) => {

      setState(() {
       current = double.parse(value.snapshot.value.toString());
      })


    });

    databaseReference.child("/PowerBox/AC/Power").once().then((value) => {

      setState(() {
        power = double.parse(value.snapshot.value.toString());
      })


    });

    databaseReference.child("/PowerBox/AC/Voltage").once().then((value) => {

      setState(() {
        voltage = double.parse(value.snapshot.value.toString());
      })


    });

    databaseReference.child("/PowerBox/AC/Energy").once().then((value) => {

      setState(() {
        energy = double.parse(value.snapshot.value.toString());
      })


    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("AC"),),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PrettyGauge(
                  minValue: 0,
                  maxValue: 240,
                  gaugeSize: 250,
                  segments: [
                    GaugeSegment('Low', 240, Colors.blue[200]!),

                  ],

                  valueWidget: Text('${voltage} V', style: TextStyle(fontSize: 20 , fontWeight: FontWeight.w400)),
                  currentValue: voltage,
                  displayWidget:
                  const Text('Voltage', style: TextStyle(fontSize: 20 , fontWeight: FontWeight.w600)),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PrettyGauge(
                  minValue: 0,
                  maxValue: 100,
                  gaugeSize: 250,
                  segments: [
                    GaugeSegment('Low', 10, Colors.blue),
                    GaugeSegment('Low', 50, Colors.orange),
                    GaugeSegment('Low', 40, Colors.red),

                  ],

                  valueWidget: Text('${current} A', style: TextStyle(fontSize: 20 , fontWeight: FontWeight.w400)),
                  currentValue: current,
                  displayWidget:
                  const Text('Current', style: TextStyle(fontSize: 20 , fontWeight: FontWeight.w600)),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PrettyGauge(
                  minValue: 0,
                  maxValue: 100,
                  gaugeSize: 250,
                  segments: [
                    GaugeSegment('Low', 100, Colors.blue),

                  ],

                  valueWidget: Text('${power} W', style: TextStyle(fontSize: 20 , fontWeight: FontWeight.w400)),
                  currentValue: power,
                  displayWidget:
                  const Text('Power', style: TextStyle(fontSize: 20 , fontWeight: FontWeight.w600)),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PrettyGauge(
                  minValue: 0,
                  maxValue: 100,
                  gaugeSize: 250,
                  segments: [
                    GaugeSegment('Low', 100, Colors.blue),

                  ],

                  valueWidget: Text('${energy} Kwh', style: TextStyle(fontSize: 20 , fontWeight: FontWeight.w400)),
                  currentValue: energy,
                  displayWidget:
                  const Text('Energy', style: TextStyle(fontSize: 20 , fontWeight: FontWeight.w600)),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
